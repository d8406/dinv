-- Dinv recipes.lua
-- Copyright Duane Robertson (duane@duanerobertson.com), 2019
-- Distributed under the LGPLv2.1 (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)


local mod = dinv
local mod_name = 'dinv'

local FLIGHT_PRIV = false
local GRAVITY_OFF = { gravity = 0.1 }
local GRAVITY_ON = { gravity = 1 }


minetest.register_craftitem(mod_name..':bag_medium', {
	description = 'Medium Bag',
	inventory_image = 'bags_medium.png',
	stack_max = 1,
	_dinv_storage_size = 16,
})

minetest.register_craft({
	output = mod_name..':bag_medium',
	recipe = {
		{'', '', ''},
		{'farming:string', mod_name..':bag_small', 'farming:string'},
		{'', mod_name..':bag_small', ''},
	},
})

minetest.register_craftitem(mod_name..':bag_large', {
	description = 'Large Bag',
	inventory_image = 'bags_large.png',
	stack_max = 1,
	_dinv_storage_size = 24,
})

minetest.register_craft({
	output = mod_name..':bag_large',
	recipe = {
		{'', '', ''},
		{'farming:string', mod_name..':bag_medium', 'farming:string'},
		{'', mod_name..':bag_medium', ''},
	},
})

minetest.register_craftitem(mod_name..':bag_small', {
	description = 'Small Bag',
	inventory_image = 'bags_small.png',
	stack_max = 1,
	_dinv_storage_size = 8,
})

minetest.register_craft({
	output = mod_name..':bag_small',
	recipe = {
		{'', 'farming:string', ''},
		{'group:wool', 'group:wool', 'group:wool'},
		{'group:wool', 'group:wool', 'group:wool'},
	},
})


minetest.register_tool(mod_name..':boots', {
	inventory_image = 'boots1_brown.png',
	description = 'Sturdy Boots',
	_dinv_armor = 0.95,
	_dinv_location = 'feet',
	_dinv_texture = 'dinv_char_boots.png',
})

minetest.register_craft({
	output = mod_name..':boots',
	recipe = {
		{'', '', ''},
		{'', 'farming:string', ''},
		{'mobs:leather', 'farming:string', 'mobs:leather'},
	},
})

minetest.register_tool(mod_name..':chain_armor', {
	inventory_image = 'chain_mail1.png',
	description = 'Chain Mail',
	_dinv_armor = 0.75,
	_dinv_location = 'body',
	_dinv_texture = 'dinv_char_chain_armor.png',
})

minetest.register_craft({
	output = mod_name..':chain_armor',
	recipe = {
		{'', mod_name..':steel_rings', ''},
		{mod_name..':steel_rings', 'mobs:leather', mod_name..':steel_rings'},
		{mod_name..':steel_rings', 'mobs:leather', mod_name..':steel_rings'},
	},
})

minetest.register_craftitem(mod_name..':diamond_plate', {
	description = 'Diamond Plate',
	inventory_image = 'dinv_diamond_plate.png',
})

minetest.register_craft({
	output = mod_name..':diamond_plate',
	recipe = {
		{'default:diamond', 'default:diamond', 'default:diamond'},
		{'default:diamond', 'default:mese_crystal', 'default:diamond'},
		{'default:diamond', 'default:diamond', 'default:diamond'},
	},
})

minetest.register_tool(mod_name..':diamond_plate_armor', {
	inventory_image = 'crystal_plate2.png',
	description = 'Diamond Plate Mail',
	_dinv_armor = 0.45,
	_dinv_location = 'body',
	_dinv_texture = 'dinv_char_diamond_plate_armor.png',
})

minetest.register_craft({
	output = mod_name..':diamond_plate_armor',
	recipe = {
		{'', mod_name..':diamond_plate', ''},
		{mod_name..':diamond_plate', mod_name..':chain_armor', mod_name..':diamond_plate'},
		{mod_name..':diamond_plate', 'default:mese', mod_name..':diamond_plate'},
	},
})

minetest.register_tool(mod_name..':fur_cloak', {
	inventory_image = 'dinv_fur_cloak.png',
	description = 'Fur Cloak',
	_dinv_armor = 0.98,
	_dinv_warmth = 2,
	_dinv_location = 'back',
	_dinv_texture = 'dinv_char_fur_cloak.png',
})

minetest.register_craft({
	output = mod_name..':fur_cloak',
	recipe = {
		{'', 'mobs:fur', ''},
		{'mobs:fur', 'farming:string', 'mobs:fur'},
		{'mobs:fur', '', 'mobs:fur'},
	},
})

minetest.register_tool(mod_name..':leather_armor', {
	inventory_image = 'leather_armour1.png',
	description = 'Leather Armor',
	_dinv_armor = 0.9,
	_dinv_location = 'body',
	_dinv_texture = 'dinv_char_leather_armor.png',
})

minetest.register_craft({
	output = mod_name..':leather_armor',
	recipe = {
		{'', 'mobs:leather', 'mobs:wax'},
		{'mobs:leather', 'farming:string', 'mobs:leather'},
		{'mobs:leather', 'farming:string', 'mobs:leather'},
	},
})

minetest.register_tool(mod_name..':leather_cap', {
	inventory_image = 'elven_leather_helm.png',
	description = 'Leather Cap',
	_dinv_armor = 0.9,
	_dinv_location = 'head',
	_dinv_texture = 'dinv_char_leather_helm.png',
})

minetest.register_craft({
	output = mod_name..':leather_cap',
	recipe = {
		{'', 'mobs:leather', ''},
		{'', 'mobs:leather', 'mobs:wax'},
		{'', 'farming:string', ''},
	},
})

minetest.register_tool(mod_name..':plate_armor', {
	inventory_image = 'plate1.png',
	description = 'Plate Mail',
	_dinv_armor = 0.6,
	_dinv_location = 'body',
	_dinv_texture = 'dinv_char_plate_armor.png',
})

minetest.register_craft({
	output = mod_name..':plate_armor',
	recipe = {
		{'', mod_name..':steel_plate', ''},
		{mod_name..':steel_plate', mod_name..':chain_armor', mod_name..':steel_plate'},
		{mod_name..':steel_plate', 'default:steel_ingot', mod_name..':steel_plate'},
	},
})

minetest.register_tool(mod_name..':steel_helmet', {
	inventory_image = 'helmet1.png',
	description = 'Steel Helmet',
	_dinv_armor = 0.8,
	_dinv_location = 'head',
	_dinv_texture = 'dinv_char_steel_helm.png',
})

minetest.register_craft({
	output = mod_name..':steel_helmet',
	recipe = {
		{'', 'default:steel_ingot', ''},
		{'default:steel_ingot', 'mobs:leather', 'default:steel_ingot'},
		{'', 'mobs:fur', ''},
	},
})

minetest.register_craftitem(mod_name..':steel_plate', {
	description = 'Steel Plate',
	inventory_image = 'dinv_steel_plate.png',
})

minetest.register_craft({
	output = mod_name..':steel_plate',
	recipe = {
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
		{'default:steel_ingot', 'default:coal_lump', 'default:steel_ingot'},
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
	},
})

minetest.register_craftitem(mod_name..':steel_rings', {
	description = 'Steel Rings',
	inventory_image = 'dinv_steel_rings.png',
})

minetest.register_craft({
	output = mod_name..':steel_rings',
	recipe = {
		{'', '', ''},
		{'default:steel_ingot', 'default:coal_lump', 'default:steel_ingot'},
		{'', 'default:steel_ingot', ''},
	},
})

minetest.register_tool(mod_name..':steel_shield', {
	inventory_image = 'lshield_dd_dk.png',
	description = 'Steel Shield',
	_dinv_armor = 0.7,
	_dinv_location = 'arm',
	_dinv_texture = 'dinv_char_steel_shield.png',
})

minetest.register_craft({
	output = mod_name..':steel_shield',
	recipe = {
		{'', mod_name..':steel_plate', ''},
		{ mod_name..':steel_plate', 'mobs:leather', mod_name..':steel_plate'},
		{'', mod_name..':steel_plate', ''},
	},
})

minetest.register_tool(mod_name..':wood_shield', {
	inventory_image = 'buckler1.png',
	description = 'Wooden Shield',
	_dinv_armor = 0.8,
	_dinv_location = 'arm',
	_dinv_texture = 'dinv_char_wood_shield.png',
})

minetest.register_craft({
	output = mod_name..':wood_shield',
	recipe = {
		{'', 'group:wood', ''},
		{'group:wood', 'mobs:leather', 'group:wood'},
		{'', 'group:wood', ''},
	},
})


minetest.register_tool(mod_name..':ring_breath', {
	inventory_image = 'coral.png',
	description = 'Ring of Breath',
	_dinv_wears_out = 91,  -- 1 hour
	_dinv_while_worn = function(player)
		player:set_breath(11)
	end,
})

minetest.register_craft({
	type = 'shapeless',
	output = mod_name..':ring_breath',
	recipe = {
		mod.magic_ingredient,
		'default:coral_skeleton',
		'default:gold_ingot',
	},
})

if FLIGHT_PRIV then
	minetest.register_tool(mod_name..':ring_flight', {
		inventory_image = 'glass.png',
		description = 'Ring of Flight',
		_dinv_wears_out = 546,  -- 10 minutes
		_dinv_on_wear = function(player)
			mod.modify_privs(player, { fly = true, noclip = 0 })
		end,
		_dinv_on_remove = function(player)
			mod.modify_privs(player, { fly = 0, noclip = 0 })
		end,
	})
end

minetest.register_tool(mod_name..':ring_leap', {
	inventory_image = 'tiger_eye.png',
	description = 'Ring of Leaping',
	_dinv_wears_out = 273,  -- 20 minutes
	_dinv_on_wear = function(player)
		player:set_physics_override(GRAVITY_OFF)
	end,
	_dinv_on_remove = function(player)
		player:set_physics_override(GRAVITY_ON)
	end,
})

minetest.register_craft({
	type = 'shapeless',
	output = mod_name..':ring_leap',
	recipe = {
		mod.magic_ingredient,
		'flowers:mushroom_red',
		'default:gold_ingot',
	},
})

minetest.register_tool(mod_name..':ring_protection_9', {
	inventory_image = 'anvil.png',
	description = 'Ring of Protection',
	_dinv_location = 'protection_finger',  -- Only one per person!
	_dinv_armor = 0.9,
})

minetest.register_craft({
	type = 'shapeless',
	output = mod_name..':ring_protection_9',
	recipe = {
		mod.magic_ingredient,
		'default:diamond',
		'default:gold_ingot',
	},
})


local trashcan_nodebox = {
	type = 'fixed',
	fixed = {
		{ -0.35, -0.5, -0.35, 0.35, 0.5, 0.35 },
	}
}

minetest.register_node(mod_name..':trashcan', {
	description = 'Trash',
	tiles = { 'default_wood.png', 'default_wood.png', 'default_wood.png^recycle.png', },
	drawtype = 'nodebox',
	node_box = trashcan_nodebox,
	groups = { choppy = 1, oddly_breakable_by_hand = 1 },
	sounds = default.node_sound_wood_defaults(),
	on_rightclick = mod.trashcan_rightclick,
	on_construct = mod.trashcan_construct,
	can_dig = mod.trashcan_can_dig,
})

minetest.register_craft({
	output = mod_name..':trashcan',
	recipe = {
		{'', 'group:leaves', ''},
		{'', 'group:wood', ''},
		{'', 'group:wood', ''},
	},
})




minetest.register_chatcommand('killunk', {
	params = '',
	description = 'Remove "unknown items"',
	privs = { },
	func = function(player_name, param)
		if not player_name then
			return
		end

		local player = minetest.get_player_by_name(player_name)
		if not player then
			return
		end

		local pos = player:get_pos()
		if not pos then
			return
		end

		local pinv = player:get_inventory()
		if not pinv then
			return
		end

		local main_size = pinv:get_size('main')
		for i = 1, main_size do
			local st = pinv:get_stack('main', i)
			local name = st:get_name()

			if not minetest.registered_items[name] then
				minetest.chat_send_player(player_name, 'Removing ' .. name)
				pinv:set_stack('main', i, nil)
			end
		end
	end,
})
