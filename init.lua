-- Dinv init.lua
-- Copyright Duane Robertson (duane@duanerobertson.com), 2019
-- Distributed under the LGPLv2.1 (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)


dinv = {}
local mod = dinv
local mod_name = 'dinv'

mod.version = '20190720'
mod.path = minetest.get_modpath(minetest.get_current_modname())
mod.world = minetest.get_worldpath()
mod.dat = {}
mod.magic_ingredient = 'default:mese_crystal'  -- Hmmm.... ?


dofile(mod.path .. '/inventory.lua')
dofile(mod.path .. '/recipes.lua')
