# dinv
Duane's Minetest Inventory

This is a basic inventory mod with recipe list, bags, and armor. It lacks a lot of the features of similar mods. All worn items, including bags and armor, are combined into one secondary inventory. Equipped bags expand your main inventory, rather than opening up extra inventories.


The source is available on github.

Code: LGPL2
Textures: strictly CC0, except for the dinv_char_\*png files

Model textures are based on:
 Jordach (CC BY-SA 3.0): character.png

Mod dependencies: default

Download: https://github.com/duane-r/dinv/archive/master.zip
